<?php
/**
 * Template part for displaying social share button
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>

<div class="share-cont">
	<button class="share-button" onclick="toggleShareButton(this)">share</button>
	<div class="share-popup-cont">
		<?php echo do_shortcode('[Sassy_Social_Share]'); ?>
	</div>
</div>

<script type="text/javascript">
//<![CDATA[
	function toggleShareButton(el) {
		jQuery(el).parent('.share-cont').toggleClass('active');
	}
//]]>
</script>