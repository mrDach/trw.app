<?php
/**
 * Displays header media
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<div class="partner">
    <a href="https://placeholder.com">
        <img src="<?php echo get_template_directory_uri() ?>/assets/images/logos/logo3.png">
    </a>
</div>
<div class="partner">
    <a href="https://placeholder.com">
        <img src="<?php echo get_template_directory_uri() ?>/assets/images/logos/logo5.png">
    </a>
</div>
<div class="partner">
    <a href="https://placeholder.com">
        <img src="<?php echo get_template_directory_uri() ?>/assets/images/logos/logo6.png">
    </a>
</div>
<div class="partner">
    <a href="https://placeholder.com">
        <img src="<?php echo get_template_directory_uri() ?>/assets/images/logos/logo4.png">
    </a>
</div>
<div class="partner">
    <a href="https://placeholder.com">
        <img src="<?php echo get_template_directory_uri() ?>/assets/images/logos/logo2.png">
    </a>
</div>
<div class="partner">
    <a href="https://placeholder.com">
        <img src="<?php echo get_template_directory_uri() ?>/assets/images/logos/logo1.png">
    </a>
</div>