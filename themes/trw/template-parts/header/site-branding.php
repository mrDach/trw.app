<?php
/**
 * Displays header site branding
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<div class="site-branding">
    <div class="site-branding-background">
        <svg viewBox="0 0 911 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                <g id="custom-header-rectangle" fill="#FEDC54">
                    <g>
                        <polygon points="0 0 584.722903 0 911 1024 0 1024"></polygon>
                    </g>
                </g>
            </g>
        </svg>
    </div>

    <?php if ( ( twentyseventeen_is_frontpage() || ( is_home() && is_front_page() ) ) && has_custom_header() ) : ?>
        <div class="featured">
            <p>Featured in:</p>
        </div>
    <?php endif; ?>

    <?php if ( ( twentyseventeen_is_frontpage() || ( is_home() && is_front_page() ) || is_page() ) && has_custom_header() ) : ?>
        <a href="#content" class="menu-scroll-down">
            <span>scroll</span>
            <?php echo twentyseventeen_get_svg( array( 'icon' => 'down' ) ); ?>
            <span class="screen-reader-text">
                <?php _e( 'Scroll down to content', 'twentyseventeen' ); ?>
            </span>
        </a>
    <?php endif; ?>

	<div class="wrap">

		<?php //the_custom_logo(); ?>

        <?php //if ( is_front_page() ) : ?>
        <div class="site-branding-logo">
            <a href="<?php echo get_home_url()?>">
            <?php fb_trw_echo_logo(); ?>
            </a>
        </div>
        <?php //endif; ?>

		<div class="site-branding-text">
			<?php if ( is_front_page() ) : ?>
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php echo get_field('title'); ?></a></h1>
			<?php else : ?>
				<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php echo get_field('title'); ?></a></p>
			<?php endif; ?>

			<?php
			$description = get_field('description');

			if ( $description || is_customize_preview() ) :
			?>
				<div class="site-description"><?php echo $description; ?></div>
			<?php endif; ?>
		</div><!-- .site-branding-text -->

	</div><!-- .wrap -->
</div><!-- .site-branding -->
