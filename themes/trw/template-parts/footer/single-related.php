<?php
/**
 * Displays related posts on single page
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

$args = array(
    'post_type' => 'post',
    'orderby'   => 'rand',
    'posts_per_page' => 3,
);
$the_query = new WP_Query( $args );
?>
<?php if ( $the_query->have_posts() ): ?>
    <div class="related-block">
        <div class="vc_grid-container-wrapper vc_clearfix">
            <div class="vc_grid-container vc_clearfix wpb_content_element vc_basic_grid blog-overview-grid">
                <div class="vc_grid vc_row vc_pageable-wrapper vc_hook_hover">
                    <div class="vc_pageable-slide-wrapper vc_clearfix">
                        <?php while ($the_query->have_posts()): ?>
                            <?php $the_query->the_post(); ?>

                            <div class="vc_grid-item vc_clearfix vc_col-sm-6 vc_grid-item-zone-c-bottom vc_visible-item">
                                <div class="vc_grid-item-mini vc_clearfix">
                                    <div class="vc_gitem-animated-block ">
                                        <div class="vc_gitem-zone vc_gitem-zone-a vc-gitem-zone-height-mode-auto vc-gitem-zone-height-mode-auto-1-1 vc_gitem-is-link"
                                            style="background-image: url('<?php echo get_the_post_thumbnail_url() ?>') !important;">
                                            <a href="<?php echo get_permalink() ?>" title="<?php echo get_the_title() ?>"
                                               class="vc_gitem-link vc-zone-link"></a>
                                            <div class="vc_gitem-zone-mini"></div>
                                        </div>
                                    </div>
                                    <div class="vc_gitem-zone vc_gitem-zone-c vc_custom_1419240516480">
                                        <div class="vc_gitem-zone-mini">
                                            <div class="vc_gitem_row vc_row vc_gitem-row-position-top">
                                                <div class="vc_col-sm-12 vc_gitem-col vc_gitem-col-align-left">
                                                    <div class="vc_custom_heading vc_gitem-post-data vc_gitem-post-data-source-post_title">
                                                        <h4 style="text-align: left"><?php echo get_the_title() ?></h4>
                                                    </div>
                                                    <div class="vc_btn3-container vc_btn3-left">
                                                        <a href="<?php echo get_permalink() ?>"
                                                            class="vc_gitem-link vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-flat vc_btn3-color-juicy-pink"
                                                            title="Read more">Read more</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="vc_clearfix"></div>
                            </div>

                        <?php endwhile; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php wp_reset_postdata(); ?>
<?php endif; ?>

