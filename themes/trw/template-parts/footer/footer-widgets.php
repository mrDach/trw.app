<?php
/**
 * Displays footer widgets if assigned
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>

<aside class="widget-area" role="complementary" aria-label="<?php esc_attr_e( 'Footer', 'twentyseventeen' ); ?>">
    <div class="footer-logo">
        <a href="<?php echo get_home_url()?>">
            <?php fb_trw_echo_logo(); ?>
        </a>
    </div>
    <div class="footer-text">
        <p>Learn from other tech entrepreneurs by joining our community!</p>
    </div>
    <div class="widget-column footer-widget">
        <?php fb_trw_print_instagram_feed();?>
    </div>
    <div class="wrap">
        <div class="wrap-fluid">
            <?php
            if ( is_active_sidebar( 'sidebar-2' ) ) { ?>
                <div class="widget-column footer-widget">
                    <?php dynamic_sidebar( 'sidebar-2' ); ?>
                </div>
            <?php } ?>
            <?php
            if ( is_active_sidebar( 'sidebar-3' ) ) { ?>
                <div class="widget-column footer-widget">
                    <?php dynamic_sidebar( 'sidebar-3' ); ?>
                </div>
            <?php } ?>
            <?php
            if ( is_active_sidebar( 'sidebar-4' ) ) { ?>
                <div class="widget-column footer-widget">
                    <?php dynamic_sidebar( 'sidebar-4' ); ?>
                </div>
            <?php } ?>
        </div>
    </div>
</aside><!-- .widget-area -->
