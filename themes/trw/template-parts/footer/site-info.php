<?php
/**
 * Displays footer site info
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<div class="site-info">
    <div class="wrap">
        <p class="contact">
            <span>Contact us</span>
            <span>
                <a id="footer_copyright_phone" href="https://www.instagram.com/explore/tags/wordcamp/">
                    <?php echo twentyseventeen_get_svg( array( 'icon' => 'phone' ) );?>
                    <?php fb_trw_customize_partial_footer_copyright_phone(); ?>
                </a>
            </span>
            <span>
                <a id="footer_copyright_envelop" href="https://www.instagram.com/explore/tags/wordcamp/">
                    <?php echo twentyseventeen_get_svg( array( 'icon' => 'envelop' ) );?>
                    <?php fb_trw_customize_partial_footer_copyright_envelop(); ?>
                </a>
            </span>
        </p>
        <p class="copyright">
            <span id="footer_copyright_text"><?php fb_trw_customize_partial_footer_copyright_text(); ?></span>
        </p>
	</div>
</div><!-- .site-info -->
