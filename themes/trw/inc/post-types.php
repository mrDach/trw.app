<?php

function fb_trw_add_team_post_type() {

    /**
     * Post Type: Expert.
     */

    $labels = array(
        "name" => __( "Experts", 'tech_ready_woman' ),
        "singular_name" => __( "Expert", 'tech_ready_woman' ),
    );

    $args = array(
        "label" => __( "Experts", 'tech_ready_woman' ),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_rest" => false,
        "rest_base" => "",
        "has_archive" => false,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "expert", "with_front" => true ),
        "query_var" => true,
        "supports" => array( "title", "thumbnail" ),
    );

    register_post_type( "expert", $args );

    /**
     * Taxonomy: Roles.
     */

    $labels = array(
        "name" => __( "Roles", 'tech_ready_woman' ),
        "singular_name" => __( "Roles", 'tech_ready_woman' ),
    );

    $args = array(
        "label" => __( "Roles", 'tech_ready_woman' ),
        "labels" => $labels,
        "public" => true,
        "hierarchical" => false,
        "show_ui" => true,
        "show_in_menu" => true,
        "show_in_nav_menus" => true,
        "query_var" => true,
        "rewrite" => array( 'slug' => 'roles', 'with_front' => true, ),
        "show_admin_column" => false,
        "show_in_rest" => false,
        "rest_base" => "",
        "show_in_quick_edit" => false,
    );
    register_taxonomy( "roles", array( "post", "expert" ), $args );
}

add_action( 'init', 'fb_trw_add_team_post_type' );

function fb_trw_add_faq_post_type()
{
    /**
     * Post Type: faq.
     */

    $labels = array(
        "name" => __( "Faq", "tech_ready_woman" ),
        "singular_name" => __( "faq", "tech_ready_woman" ),
    );

    $args = array(
        "label" => __( "faq", "tech_ready_woman" ),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_rest" => false,
        "rest_base" => "",
        "has_archive" => false,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "faq", "with_front" => true ),
        "query_var" => true,
        "supports" => array( "title", "editor" ),
    );

    register_post_type( "faq", $args );

    /**
     * Taxonomy: FAQ_categories.
     */

    $labels = array(
        "name" => __( "Faq Categories", "tech_ready_woman" ),
        "singular_name" => __( "faq_categories", "tech_ready_woman" ),
    );

    $args = array(
        "label" => __( "faq_categories", "tech_ready_woman" ),
        "labels" => $labels,
        "public" => true,
        "hierarchical" => false,
        "show_ui" => true,
        "show_in_menu" => true,
        "show_in_nav_menus" => true,
        "query_var" => true,
        "rewrite" => array( 'slug' => 'faq_categories', 'with_front' => true, ),
        "show_admin_column" => false,
        "show_in_rest" => false,
        "rest_base" => "",
        "show_in_quick_edit" => false,
    );
    register_taxonomy( "faq_categories", array( "faq" ), $args );
}

add_action( 'init', 'fb_trw_add_faq_post_type' );

function fb_trw_gitem_template_attribute_post_categories( $value, $data ) {

    /**
     * @var null|Wp_Post $post ;
     * @var string $data ;
     */
    extract( array_merge( array(
        'post' => null,
        'data' => '',
    ), $data ) );
    $atts_extended = array();
    parse_str( $data, $atts_extended );

    if($post->post_type !== 'post' && in_category( 'Testimonials' )){
        return $value;
    } else{
        return fb_trw_get_roles(array(
            'post' => $post,
            'atts' => $atts_extended['atts'],
        ) );
    }
}
add_filter( 'vc_gitem_template_attribute_post_categories', 'fb_trw_gitem_template_attribute_post_categories', 11, 2 );

function fb_trw_get_roles($atts){
    /**
     * @var $vc_btn WPBakeryShortCode_VC_Gitem_Post_Categories
     * @var $post WP_Post
     * @var $atts
     *
     */
    VcShortcodeAutoloader::getInstance()->includeClass( 'WPBakeryShortCode_VC_Gitem_Post_Categories' );

    $categories = get_the_terms( false, 'roles' );
    if ( ! $categories || is_wp_error( $categories ) )
        $categories = array();

    $categories = array_values( $categories );

    foreach ( array_keys( $categories ) as $key ) {
        _make_cat_compat( $categories[$key] );
    }

    $separator = '';
    $css_class = array( 'vc_gitem-post-data' );
    $css_class[] = vc_shortcode_custom_css_class( $atts['css'] );
    $css_class[] = $atts['el_class'];
    $css_class[] = 'vc_gitem-post-data-source-post_categories';
    $style = str_replace( ',', 'comma', $atts['category_style'] );
    $output = '<div class="' . esc_attr( implode( ' ', array_filter( $css_class ) ) ) . ' vc_grid-filter vc_clearfix vc_grid-filter-' . esc_attr( $style ) . ' vc_grid-filter-size-' . esc_attr( $atts['category_size'] ) . ' vc_grid-filter-center vc_grid-filter-color-' . esc_attr( $atts['category_color'] ) . '">';
    $data = array();
    if ( ! empty( $categories ) ) {
        foreach ( $categories as $category ) {
            $category_link = '';
            if ( ! empty( $atts['link'] ) ) {
                $category_link = 'href="' . esc_url( get_category_link( $category->term_id ) ) . '" alt="' . esc_attr( sprintf( __( 'View all posts in %s', 'js_composer' ), $category->name ) ) . '"';
            }

            $wrapper = '<div class="vc_grid-filter-item vc_gitem-post-category-name">';
            $content = esc_html( $category->name );
            if ( ! empty( $category_link ) ) {
                $content = '<span class="vc_gitem-post-category-name"><a ' . $category_link . ' class="vc_gitem-link">' . $content . '</a>' . '</span>';
            } else {
                $content = '<span class="vc_gitem-post-category-name">' . $content . '</span>';
            }
            $wrapper_end = '</div>';
            $data[] = $wrapper . $content . $wrapper_end;
        }
    }
    if ( empty( $atts['category_style'] ) || ' ' === $atts['category_style'] || ', ' === $atts['category_style'] ) {
        $separator = $atts['category_style'];
    }
    $output .= implode( $separator, $data );
    $output .= '</div>';

    return $output;
}

function fb_trw_gitem_attribute_featured_image_img( $img ) {

    $img['thumbnail'] = $img['thumbnail'].'<div class="read-more">READ STORY</div>';

    return $img;

}

add_filter( 'vc_gitem_attribute_featured_image_img', 'fb_trw_gitem_attribute_featured_image_img' );