<?php

/**
 * Register shortcode settings
 * @param $shortcodes - all shortcodes
 * @return array
 */
function fb_trw_acf_link( $shortcodes ) {

    $groups = function_exists( 'acf_get_field_groups' ) ? acf_get_field_groups() : apply_filters( 'acf/get_field_groups', array() );
    $groups_param_values = $fields_params = array();
    foreach ( $groups as $group ) {
        $id = isset( $group['id'] ) ? 'id' : ( isset( $group['ID'] ) ? 'ID' : 'id' );
        $groups_param_values[ $group['title'] ] = $group[ $id ];
        $fields = function_exists( 'acf_get_fields' ) ? acf_get_fields( $group[ $id ] ) : apply_filters( 'acf/field_group/get_fields', array(), $group[ $id ] );
        $fields_param_value = array();
        foreach ( (array) $fields as $field ) {
            $fields_param_value[ $field['label'] ] = (string) $field['key'];
        }
        $fields_params[] = array(
            'type' => 'dropdown',
            'heading' => __( 'Field name', 'js_composer' ),
            'param_name' => 'field_from_' . $group[ $id ],
            'value' => $fields_param_value,
            'save_always' => true,
            'description' => __( 'Select field from group.', 'js_composer' ),
            'dependency' => array(
                'element' => 'field_group',
                'value' => array( (string) $group[ $id ] ),
            ),
        );
    }

    $shortcode = array(
        'name' => __( 'ACF Link', 'tech_ready_woman' ),
        'base' => 'vc_fb_trw_acf_link',
        'category' => __( 'Content', 'tech_ready_woman' ),
        'params' => array_merge(
            array(
                array(
                    'type' => 'dropdown',
                    'heading' => __( 'Field group', 'js_composer' ),
                    'param_name' => 'field_group',
                    'value' => $groups_param_values,
                    'save_always' => true,
                    'description' => __( 'Select field group.', 'js_composer' ),
                ),
            ), $fields_params,
            array(
                array(
                    'type' => 'textfield',
                    'heading' => __( 'Text', 'js_composer' ),
                    'param_name' => 'link_text',
                ),
                array(
                    'type' => 'textfield',
                    'heading' => __( 'Extra class name', 'js_composer' ),
                    'param_name' => 'el_class',
                    'description' => __( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'js_composer' ),
                ),
            )
        ),
        'post_type' => Vc_Grid_Item_Editor::postType(),
    );
    $shortcodes['vc_fb_trw_acf_link'] = $shortcode;
    return $shortcodes;
}
add_filter( 'vc_grid_item_shortcodes', 'fb_trw_acf_link', 11, 1 );

/**
 * Function for displaying
 *
 * @param array $atts    - the attributes of shortcode
 * @param string $content - the content between the shortcodes tags
 *
 * @return string $html - the HTML content for this shortcode.
 */
function fb_trw_acf_link_function( $atts, $content ) {
    $field = null;
    if( isset($atts['field_group']) && isset($atts['field_from_' . $atts['field_group']]) ){
        $field = $atts['field_from_' . $atts['field_group']];
    }

    $class = array('vc_gitem-acf-link');
    if( isset($atts['el_class']) ){
        $class[] = $atts['el_class'];
    }

    $text = __( 'View Profile', 'tech_ready_woman' );
    if( isset($atts['link_text']) ){
        $text = $atts['link_text'];
    }

    $html = '';
    if( $field ){
        $class[] = $field;
        $html .= '<a href="{{ acf:'.$atts['field_from_' . $atts['field_group']].' }}" '.$field.' class="' . implode(' ', $class) . '">' . $text . '</a>';
    }

    return $html;
}
add_shortcode( 'vc_fb_trw_acf_link', 'fb_trw_acf_link_function' );