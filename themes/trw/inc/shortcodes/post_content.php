<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 28.12.17
 * Time: 12:36
 */

/**
 * Register shortcode settings
 * @param $shortcodes - all shortcodes
 * @return array
 */
function vc_fb_trw_post_content( $shortcodes ) {
    $shortcode = array(
        'name' => __( 'Post content', 'tech_ready_woman' ),
        'base' => 'vc_fb_trw_post_content',
        'category' => __( 'Content', 'tech_ready_woman' ),
        'params' => array_merge(
            array(
                array(
                    'type' => 'textfield',
                    'heading' => __( 'Tag', 'js_composer' ),
                    'param_name' => 'tag',
                ),
                array(
                    'type' => 'textfield',
                    'heading' => __( 'Extra class name', 'js_composer' ),
                    'param_name' => 'el_class',
                    'description' => __( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'js_composer' ),
                ),
            )
        ),
        'post_type' => Vc_Grid_Item_Editor::postType(),
    );
    $shortcodes['vc_fb_trw_post_content'] = $shortcode;
    return $shortcodes;
}
add_filter( 'vc_grid_item_shortcodes', 'vc_fb_trw_post_content', 11, 1 );

/**
 * Function for displaying
 *
 * @param array $atts    - the attributes of shortcode
 * @param string $content - the content between the shortcodes tags
 *
 * @return string $html - the HTML content for this shortcode.
 */
function vc_fb_trw_post_content_function( $atts, $content ) {

    $class = ['vc_gitem-post-data', 'vc_gitem-post-data-source-post_content'];
    if( isset($atts['el_class']) ){
        $class[] = $atts['el_class'];
    }

    $tag = 'div';
    if( isset($atts['tag']) ){
        $tag = $atts['tag'];
    }

    $html = '';
    $html .= "<$tag class=\"". implode(' ', $class) ."\">{{ post_data:post_content }}</$tag>";

    return $html;
}
add_shortcode( 'vc_fb_trw_post_content', 'vc_fb_trw_post_content_function' );