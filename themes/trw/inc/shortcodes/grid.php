<?php
$trw_grid = false;

function add_trw_grid($attr) {
    $default = array(
        'post_type' => 'post',
        'grid_template' => 0,
        'element_width' => 4,
        'gap' => 30,
        'class' => '',
        'orderby' => 'date',
        'order' => 'DESC',
        'per_page' => -1, // -1 => show all elements without pagination
    );
    $attrVal = shortcode_atts($default, $attr);
    if (!$attrVal['post_type'] || !$attrVal['grid_template']) {
        return '';
    }
    return trw_grid_render($attrVal);
}
add_shortcode('trw_grid', 'add_trw_grid');

function trw_grid_render($params, $page = 1, $isAjax = false) {
    global $trw_grid;
    $trw_grid = true;
    $ajaxResult = array();
    $output = '';
    $queryParam = array(
        'post_type' => $params['post_type'],
        'posts_per_page' => $params['per_page'],
        'orderby' => $params['orderby'],
        'order' => $params['order'],
        'paged' => $page,
    );
    $query = new WP_Query($queryParam);
    if ($query->have_posts()) {
        require_once vc_path_dir( 'PARAMS_DIR', 'vc_grid_item/class-vc-grid-item.php' );
        $grid_item = new Vc_Grid_Item();
        $grid_item->setGridAttributes(array(
            'element_width' => $params['element_width']
        ));
        $grid_item->setTemplateById( $params['grid_template'] );
        ob_start();
        ?>
        <?php if (!$isAjax): ?>
            <div class="vc_grid-container-wrapper vc_clearfix trw-grid-cont" data-trw-grid-params='<?php echo json_encode($params)?>'>
                <div class="vc_grid-container vc_clearfix wpb_content_element vc_basic_grid <?php echo $params['class'] ?>">
                    <?php echo $grid_item->addShortcodesCustomCss() ?>
                    <div class="vc_grid vc_row <?php echo $params['gap'] ? 'vc_grid-gutter-' . $params['gap'] . 'px' : '' ?> vc_pageable-wrapper vc_hook_hover">
                        <div class="vc_pageable-slide-wrapper vc_clearfix trw-grid-inner-cont">
        <?php endif; ?>
                            <?php while ($query->have_posts()):
                                $query->the_post();
                                $htmlItem = $grid_item->renderItem( get_post() );
                                if ($isAjax) {
                                    $ajaxResult[] = $htmlItem;
                                } else {
                                    echo $htmlItem;
                                }
                            endwhile;
                            wp_reset_postdata();?>
        <?php if (!$isAjax): ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <?php
        $output .= ob_get_clean();
    }
    $trw_grid = false;
    return $isAjax ? json_encode($ajaxResult) : $output;
}

function vc_gitem_template_attribute_filter_terms_css_classes_trw_grid($value, $data) {
    global $trw_grid;
    return $value . ($trw_grid ? ' vc_visible-item' : '');
}
add_filter( 'vc_gitem_template_attribute_filter_terms_css_classes', 'vc_gitem_template_attribute_filter_terms_css_classes_trw_grid', 11, 2 );

function fb_trw_register_vc_trw_grid() {
    $posts = get_posts( array(
        'posts_per_page'    => -1,
        'post_type'         => 'vc_grid_item',
    ));
    $gridTemplates = array();
    foreach ( $posts as $post ) {
        $gridTemplates[] = array(
            'label' => $post->post_title,
            'value' => $post->ID,
        );
    }
    return array(
        'name' => __( 'TRW Grid', 'tech_ready_woman' ),
        'base' => 'trw_grid',
        'category' => __( 'Content', 'tech_ready_woman' ),
        'params' => array(
            array(
                'type'          => 'dropdown',
                'heading'       => esc_html__( 'Data source', 'tech_ready_woman' ),
                'param_name'    => 'post_type',
                'value'         => get_post_types_for_trw_grid(),
                'description'   => esc_html__( 'Select content type for your grid.', 'tech_ready_woman' ),
            ),
            array(
                'type'          => 'dropdown',
                'heading'       => esc_html__( 'Grid element template', 'tech_ready_woman' ),
                'param_name'    => 'grid_template',
                'value'         => $gridTemplates,
            ),
            array(
                'type' => 'dropdown',
                'heading' => __( 'Grid elements per row', 'tech_ready_woman' ),
                'param_name' => 'element_width',
                'value' => array(
                    array(
                        'label' => '6',
                        'value' => 2,
                    ),
                    array(
                        'label' => '4',
                        'value' => 3,
                    ),
                    array(
                        'label' => '3',
                        'value' => 4,
                    ),
                    array(
                        'label' => '2',
                        'value' => 6,
                    ),
                    array(
                        'label' => '1',
                        'value' => 12,
                    ),
                ),
                'std' => '4',
                'edit_field_class' => 'vc_col-sm-6',
                'description' => __( 'Select number of single grid elements per row.', 'tech_ready_woman' ),
            ),
            array(
                'type' => 'dropdown',
                'heading' => __( 'Gap', 'tech_ready_woman' ),
                'param_name' => 'gap',
                'value' => array(
                    '0px' => '0',
                    '1px' => '1',
                    '2px' => '2',
                    '3px' => '3',
                    '4px' => '4',
                    '5px' => '5',
                    '10px' => '10',
                    '15px' => '15',
                    '20px' => '20',
                    '25px' => '25',
                    '30px' => '30',
                    '35px' => '35',
                ),
                'std' => '30',
                'description' => __( 'Select gap between grid elements.', 'tech_ready_woman' ),
                'edit_field_class' => 'vc_col-sm-6',
            ),
            array(
                'type' => 'dropdown',
                'heading' => __( 'Order by', 'tech_ready_woman' ),
                'param_name' => 'orderby',
                'value' => array(
                    __( 'Date', 'tech_ready_woman' ) => 'date',
                    __( 'Order by post ID', 'tech_ready_woman' ) => 'ID',
                    __( 'Author', 'tech_ready_woman' ) => 'author',
                    __( 'Title', 'tech_ready_woman' ) => 'title',
                    __( 'Last modified date', 'tech_ready_woman' ) => 'modified',
                    __( 'Post/page parent ID', 'tech_ready_woman' ) => 'parent',
                    __( 'Number of comments', 'tech_ready_woman' ) => 'comment_count',
                    __( 'Menu order/Page Order', 'tech_ready_woman' ) => 'menu_order',
                    __( 'Random order', 'tech_ready_woman' ) => 'rand',
                ),
                'param_holder_class' => 'vc_grid-data-type-not-ids',
                'dependency' => array(
                    'element' => 'post_type',
                    'value_not_equal_to' => array(
                        'ids',
                        'custom',
                    ),
                ),
            ),
            array(
                'type' => 'dropdown',
                'heading' => __( 'Sort order', 'tech_ready_woman' ),
                'param_name' => 'order',
                'value' => array(
                    __( 'Descending', 'tech_ready_woman' ) => 'DESC',
                    __( 'Ascending', 'tech_ready_woman' ) => 'ASC',
                ),
                'param_holder_class' => 'vc_grid-data-type-not-ids',
                'dependency' => array(
                    'element' => 'post_type',
                    'value_not_equal_to' => array(
                        'ids',
                        'custom',
                    ),
                ),
            ),
            array(
                'type'          => 'textfield',
                'heading'       => esc_html__( 'Elements per page', 'tech_ready_woman' ),
                'param_name'    => 'per_page',
                'value'         => '-1',
                'description'   => esc_html__( 'If -1 then all elements will be shown.', 'tech_ready_woman' ),
            ),
            array(
                'type'          => 'textfield',
                'heading'       => esc_html__( 'Extra class name', 'tech_ready_woman' ),
                'param_name'    => 'class',
                'value'         => '',
                'description'   => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'tech_ready_woman' ),
            ),
        )
    );
}
vc_lean_map( 'trw_grid', 'fb_trw_register_vc_trw_grid' );

function get_post_types_for_trw_grid() {
    $postTypes = array();
    $excludedPostTypes = array(
        'revision',
        'nav_menu_item',
        'vc_grid_item',
    );
    $postTypesList = get_post_types( array(), 'objects' );
    foreach ( $postTypesList  as $post_type ) {
        if ( ! in_array( $post_type->name, $excludedPostTypes ) ) {
            $postTypes[] = array(
                'label' => $post_type->label,
                'value' => $post_type->name,
            );
        }
    }
    return $postTypes;
}

function trw_grid_ajax_enqueue() {
    wp_enqueue_script(
        'trw-grid-script',
        get_template_directory_uri() . '/assets/js/trw-grid-script.js',
        array('jquery'),
        false,
        true
    );
    wp_localize_script(
        'trw-grid-script',
        'trw_grid_params',
        array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) )
    );
}
add_action( 'wp_enqueue_scripts', 'trw_grid_ajax_enqueue' );

function trw_grid_ajax() {
    if (isset($_GET) && isset($_GET['post_type']) && isset($_GET['grid_template']) && isset($_GET['element_width']) && isset($_GET['per_page']) && isset($_GET['page'])) {
        $params = array(
            'post_type' => strval($_GET['post_type']),
            'grid_template' => intval($_GET['grid_template']),
            'element_width' => intval($_GET['element_width']),
            'orderby' => strval($_GET['orderby']),
            'order' => strval($_GET['order']),
            'per_page' => intval($_GET['per_page']),
        );
        echo trw_grid_render($params, intval($_GET['page']), true);
    }
    die();
}
add_action( 'wp_ajax_trw_grid_ajax', 'trw_grid_ajax' );
add_action( 'wp_ajax_nopriv_trw_grid_ajax', 'trw_grid_ajax' );