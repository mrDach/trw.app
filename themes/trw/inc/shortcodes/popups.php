<?php

function fb_trw_add_popup_post_type() {

    /**
     * Post Type: Popup.
     */

    $labels = array(
        "name" => __( "Popup", 'tech_ready_woman' ),
        "singular_name" => __( "Popup", 'tech_ready_woman' ),
    );

    $args = array(
        "label" => __( "Popup", 'tech_ready_woman' ),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_rest" => false,
        "rest_base" => "",
        "has_archive" => false,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "popup", "with_front" => true ),
        "query_var" => true,
        "supports" => array( "title", "editor", "thumbnail" ),
    );

    register_post_type( "popup", $args );
}
add_action( 'init', 'fb_trw_add_popup_post_type' );

$trw_popups = array();

function add_trw_popup($attr, $content = null) {
    global $trw_popups;
    $default = array(
        'popup_id' => 0,
        'class' => '',
        'class_popup' => '',
        'tag' => 'span',
    );
    $attrVal = shortcode_atts($default, $attr);
    $content = do_shortcode($content);
    if (!$attrVal['popup_id']) {
        return $content;
    }

    $trw_popups[ $attrVal['popup_id'] ] = $attrVal;
    return '<' . $attrVal['tag']
        . ' class="trw-popup-tag '. $attrVal['class']
        .'" data-popup-id="' . $attrVal['popup_id'] . '">'
            . $content
    . '</' . $attrVal['tag'] . '>';
}
add_shortcode('trw_popup', 'add_trw_popup');

function trw_popup_render() {
    global $trw_popups;
    if (empty($trw_popups)) {
        return;
    }
    $the_query = new WP_Query(array(
        'post_type' => 'popup',
        'post__in' => array_keys($trw_popups),
    ));
    if (!$the_query->have_posts() ) {
        return;
    }
    while ($the_query->have_posts()): ?>
        <?php $the_query->the_post(); ?>
        <?php if (!isset($trw_popups[ get_the_ID() ])) continue; ?>
        <?php $attr = $trw_popups[ get_the_ID() ]; ?>

        <div class="trw-popup-cont <?php echo $attr['class_popup'] ?>" id="trw-popup-<?php echo $attr['popup_id'] ?>">
            <div class="trw-popup-overlay"></div>
            <div class="trw-popup-window-cont">
                <div class="trw-popup-overlay trw-popup-trans"></div>
                <div class="trw-popup-window">
                    <button class="trw-popup-close"></button>
                    <div class="trw-popup-content"><?php the_content() ?></div>
                </div>
            </div>
        </div>
    <?php endwhile; ?>
    <script type="text/javascript">
        //<![CDATA[
        (function($){
            $(document).ready(function(){
                var popupIds = [];
                $('[data-popup-id]').each(function(index, elem){
                    var popupTag = $(elem);
                    var popupId = popupTag.attr('data-popup-id');
                    if (!popupId) {
                        return;
                    }
                    var closeHandler = function(e){
                        var cont = $(this).parents('.trw-popup-cont');
                        if (cont.hasClass('trw-popup-active')) {
                            cont.removeClass('trw-popup-active');
                        }
                        if (!$('.trw-popup-active').length) {
                            $(document.body).css('overflow', 'initial');
                        }
                        $(document).off('keydown.trw-popup-'+popupId);
                    };
                    var keydownHandler = function(e) {
                        if (e.which == 27) { // escape key
                            closeHandler.call($('#trw-popup-'+popupId+' .trw-popup-close'));
                        }
                    };
                    if (popupIds.indexOf(popupId) === -1) {
                        popupIds.push(popupId);
                        var cont = $('#trw-popup-' + popupId);
                        if (cont.length) {
                            $('.trw-popup-overlay', cont).click(closeHandler);
                            $('.trw-popup-close', cont).click(closeHandler);
                        }
                    }
                    $(elem).click(function(e){
                        var cont = $('#trw-popup-' + popupId);
                        if (cont.length) {
                            if (!$('.trw-popup-active').length) {
                                $(document.body).css('overflow', 'hidden');
                            }
                            if (!cont.hasClass('trw-popup-active')) {
                                cont.addClass('trw-popup-active');
                            }
                            $('.trw-popup-close', cont).focus();
                            $(document).on('keydown.trw-popup-'+popupId, keydownHandler);
                        }
                    });
                });
            });
        })(jQuery);
        // ]]>
    </script>
    <?php wp_reset_postdata();
}
add_action( 'wp_footer', 'trw_popup_render' );

/**
 * Register shortcode settings
 * @param void
 * @return void
 */
function fb_trw_component_title() {
    // Title
    vc_map(
        array(
            'name' => __( 'Pop Up', 'tech_ready_woman' ),
            'base' => 'trw_popup',
            'category' => __( 'Content', 'tech_ready_woman' ),
            'params' => array(
                array(
                    'type'          => 'autocomplete',
                    'class'         => '',
                    'heading'       => esc_html__( 'Popup Name', 'tech_ready_woman' ),
                    'param_name'    => 'popup_id',
                    'settings'      => array( 'values' => fb_trw_get_type_posts_data('popup') ),
                ),
                array(
                    'type'          => 'textfield',
                    'class'         => '',
                    'heading'       => esc_html__( 'Tag name', 'tech_ready_woman' ),
                    'param_name'    => 'tag',
                    'value'         => esc_html__( 'span', 'tech_ready_woman' ),
                    'description'   => esc_html__( 'Tag name of wrapper element. Default: span', 'tech_ready_woman' ),
                ),
                array(
                    'type' => 'textarea_html',
                    'holder' => 'div',
                    'heading' => __( 'Text', 'js_composer' ),
                    'param_name' => 'content',
                ),
                array(
                    'type'          => 'textfield',
                    'class'         => '',
                    'heading'       => esc_html__( 'Additional class for wrapper', 'tech_ready_woman' ),
                    'param_name'    => 'class',
                    'value'         => '',
                ),
                array(
                    'type'          => 'textfield',
                    'class'         => '',
                    'heading'       => esc_html__( 'Additional class for popup', 'tech_ready_woman' ),
                    'param_name'    => 'class_popup',
                    'value'         => '',
                ),
            )
        )
    );
}
add_action( 'vc_before_init', 'fb_trw_component_title' );

/**
 * Get all type posts
 * @param string $post_type - registered post type
 * @return array
 **/
function fb_trw_get_type_posts_data( $post_type = 'post' ) {

    $posts = get_posts( array(
        'posts_per_page'    => -1,
        'post_type'         => $post_type,
    ));

    $result = array();
    foreach ( $posts as $post ) {
        $result[] = array(
            'value' => $post->ID,
            'label' => $post->post_title,
        );
    }
    return $result;
}