<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 20.12.17
 * Time: 12:44
 */

function fb_trw_get_instagram_photos(){
    $otherPage = get_theme_mod( 'instagram_user' );
    if (!isset($otherPage) || empty($otherPage) || !$otherPage){
        return [];
    }
    $profileUrl = "https://www.instagram.com/$otherPage/?__a=1";
    $thumbnails = [];

    $iterationUrl = $profileUrl;
    $tryNext = true;
    $limit = 36; // a multiple of 6
    $found = 0;
    while ($tryNext) {
        $tryNext = false;
        $response = file_get_contents($iterationUrl);
        if ($response === false) {
            break;
        }
        $data = json_decode($response, true);
        if ($data === null) {
            break;
        }
        $media = $data['user']['media'];
        $found += count($media['nodes']);
        foreach ($media['nodes'] as $node){
            $thumbnails[] = [
                'src' => $node['thumbnail_resources'][count($node['thumbnail_resources'])-1]['src'],
                'link' => "https://www.instagram.com/p/{$node['code']}/",
            ];
        }
        //var_dump($node);
        if ($media['page_info']['has_next_page'] && $found < $limit) {
            $iterationUrl = $profileUrl . '&max_id=' . $media['page_info']['end_cursor'];
            $tryNext = true;
        }
    }
    return $thumbnails;
}

function fb_trw_print_instagram_feed(){
    $photos = fb_trw_get_instagram_photos();
    ?>
    <div class="instagram">
        <?php foreach ($photos as $photo):?>

        <a href="<?php echo $photo['link'];?>"><img src="<?php echo $photo['src'];?>"></a>

        <?php endforeach;?>
    </div>
    <?php
}
