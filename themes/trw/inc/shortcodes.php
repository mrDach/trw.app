<?php

require_once get_parent_theme_file_path('inc/shortcodes/acf_link.php');
require_once get_parent_theme_file_path('inc/shortcodes/popups.php');
require_once get_parent_theme_file_path('inc/shortcodes/post_content.php');
require_once get_parent_theme_file_path('inc/shortcodes/grid.php');
