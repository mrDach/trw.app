(function($){
    $(document).ready(function(){
        $('[data-trw-grid-params]').each(function(index, elem) {
            var wrapper = $(elem);
            var params = JSON.parse(wrapper.attr('data-trw-grid-params'));
            var innerCont = $('.trw-grid-inner-cont', wrapper);
            if (!innerCont.length || !isValidParams(params)) {
                return;
            }
            if (parseInt(params.per_page) === -1) {
                return;
            }
            var sampleItem = $('.vc_grid-item:first', innerCont);
            if (!sampleItem || !sampleItem.length) {
                return;
            }
            var marker = sampleItem.clone();
            marker.html('');
            marker.addClass('trw-grid-marker');
            marker.appendTo(innerCont);
            wrapper.attr('data-trw-grid-page', 1);
        });
        $(window).scroll(lazyLoad);
        lazyLoad();
    });

    function lazyLoad() {
        var windowTop = $(window).scrollTop();
        var windowBottom = windowTop + $(window).height();
        $('.trw-grid-cont:not(.trw-grid-completely-loaded) .trw-grid-marker').each(function(){
            var marker = $(this);
            var loading = parseInt(marker.attr('data-trw-grid-loading'));
            if (loading) {
                return;
            }
            var objTop = marker.offset().top;
            var objBottom = objTop + marker.height();
            if (windowTop <= objBottom && windowBottom >= objTop) {
                var wrapper = marker.parents('[data-trw-grid-params]');
                var params = JSON.parse(wrapper.attr('data-trw-grid-params'));
                if (!isValidParams(params)) {
                    return;
                }
                var page = parseInt(wrapper.attr('data-trw-grid-page'));
                marker.attr('data-trw-grid-loading', 1);
                $.ajax({
                    url: trw_grid_params.ajaxurl,
                    dataType: 'json',
                    data: {
                        'action': 'trw_grid_ajax',
                        'post_type': params.post_type,
                        'grid_template': params.grid_template,
                        'element_width': params.element_width,
                        'orderby': params.orderby,
                        'order': params.order,
                        'per_page': params.per_page,
                        'page': ++page
                    },
                    success: function(data) {
                        if (data && data.length) {
                            for (var i = 0; i < data.length; i++) {
                                var item = $(data[i]).filter('.vc_grid-item');
                                item.insertBefore(marker);
                            }
                            wrapper.attr('data-trw-grid-page', page);
                        } else {
                            if (!wrapper.hasClass('trw-grid-completely-loaded')) {
                                wrapper.addClass('trw-grid-completely-loaded');
                            }
                        }
                        marker.attr('data-trw-grid-loading', 0);
                        lazyLoad();
                    },
                    error: function(error){
                        console.log(error);
                        marker.attr('data-trw-grid-loading', 0);
                        lazyLoad();
                    }
                });
            }
        });
    }

    function isValidParams(params) {
        return trw_grid_params.ajaxurl &&
            typeof params.post_type &&
            typeof params.grid_template !== 'undefined' &&
            typeof params.element_width !== 'undefined' &&
            typeof params.orderby !== 'undefined' &&
            typeof params.order !== 'undefined' &&
            typeof params.per_page !== 'undefined';
    }
})(jQuery);