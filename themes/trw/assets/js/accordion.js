(function( $ ) {

  var $body,
    $accordionLinks,
    itemClass = '.vc_grid-item',
    titleClass = '.vc_custom_heading.accordion.vc_gitem-post-data.vc_gitem-post-data-source-post_title';

  function init() {
    // Variables and DOM Caching.
    $body = $( 'body' );
  }

  // Fire on document ready.
  $( document ).ready( function() {

    init();
    $body.on('click', titleClass, function (e) {
      e.preventDefault();
      $(this).parents(itemClass).toggleClass('accordion-open');
    });
  });

})( jQuery );