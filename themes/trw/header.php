<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php
wp_head();
global $post;
?>
</head>

<body <?php if(isset($post)) body_class($post->post_name); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'twentyseventeen' ); ?></a>

	<header id="masthead" class="site-header" role="banner">
		<?php if ( is_page() && !( is_page_template( 'page-without-header.php' ) || is_page_template( 'page-without-header-no-wrapper.php' ) || is_page_template( 'page-without-header-no-wrapper-no-footer-widgets.php' ) ) ) : ?>

            <?php get_template_part( 'template-parts/header/header', 'image' ); ?>

            <?php if ( is_page() && is_front_page() ) : ?>
                <div class="partners-top">
                    <div class="wrap-fluid">
                        <?php get_template_part( 'template-parts/header/header', 'partners' ); ?>
                    </div><!-- .wrap -->
                </div><!-- .partners-top -->
		    <?php endif; ?>
		<?php endif; ?>

        <?php if ( has_nav_menu( 'top' ) ) : ?>
			<div class="navigation-top">
				<div class="wrap-fluid">
					<?php get_template_part( 'template-parts/navigation/navigation', 'top' ); ?>
				</div><!-- .wrap -->
			</div><!-- .navigation-top -->
		<?php endif; ?>

		<?php if ( is_single() && get_post_type() === 'post') : ?>
			<div class="vc_wp_custommenu wpb_content_element blog-menu">
				<div class="widget widget_nav_menu">
					<?php wp_nav_menu( array(
						'theme_location' => 'blog',
						'menu_id'        => 'blog-menu'
					) ); ?>
				</div>
			</div>
			<?php echo do_shortcode('[vc_row][vc_column][vc_wp_categories title="" el_class="categories-top-widget"][/vc_column][/vc_row]'); ?>
		<?php endif; ?>
	</header><!-- #masthead -->

	<?php

	/*
	 * If a regular post or page, and not the front page, show the featured image.
	 * Using get_queried_object_id() here since the $post global may not be set before a call to the_post().
	 */
	if ( ( is_single() || ( is_page() && ! twentyseventeen_is_frontpage() ) ) && has_post_thumbnail( get_queried_object_id() ) ) : ?>
		<div class="single-featured-image-header">
			<div class="wrap">
				<?php if ( is_single() && has_post_thumbnail( get_queried_object_id() ) ) : ?>
					<?php get_template_part( 'template-parts/header/social', 'share' ); ?>
				<?php endif; ?>

				<?php echo get_the_post_thumbnail( get_queried_object_id(), 'twentyseventeen-featured-image' ); ?>
			</div>
		</div><!-- .single-featured-image-header -->
	<?php endif; ?>

	<div class="site-content-contain">

		<div id="content" class="site-content">
